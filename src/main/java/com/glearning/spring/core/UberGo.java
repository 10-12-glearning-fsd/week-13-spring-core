package com.glearning.spring.core;

public interface UberGo {
	
	void trip(String source, String destination);

}
